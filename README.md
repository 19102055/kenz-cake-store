# Kenz Cake Store



***




## Name
Kenz Cake Store

## Description
Kenz Cake Store ini merupakan toko yang bergerak dalam penjualan 
produk patiseri. Selain itu, kemampuan untuk mengakses informasi dan melakukan transaksi melalui website juga membuka peluang bagi bisnis untuk menjangkau calon pelanggan di berbagai wilayah. 
Dengan adanya aksesibilitas ini, bisnis dapat memperluas pangsa pasar dan meningkatkan potensi pertumbuhan bisnisnya, yang mana dapat dilakukan sebuah pertukaran informasi dan data mengenai produk yang ada di Kenz Cake 
Store.



## Penggunaan
- Halaman depan merupakan halaman pertama yang akan muncul ketika 
pengguna mengakses ke website
- Halaman login yang merupakan sebuah halaman 
yang dimana pelanggan melakukan autentikasi sebelum dapat masuk 
menggunakan akun yang sudah ada, halaman ini akan meminta pelanggan 
untuk memasukan informasi untuk dapat melakukan login seperti halnya
email dan password
- Ketika pengguna tidak dapat mengingat kata sandi, 
pengguna dapat mengakses halaman lupa password untuk memulai proses 
pemulihan kata sandi. 
- Halaman register adalah
area pada sebuah sistem website yang memungkinkan pengguna untuk 
membuat akun baru.
- Halaman transaksi memungkinkan pengguna untuk halaman ini digunakan 
untuk pengguna melihat transaksi yang sedang atau pernah dilakukan
- Halaman detail transaksi pengguna 
dapat melihat semua detail terkait transaksi tertentu, seperti produk atau 
layanan yang dibeli, jumlah yang dibayarkan, tanggal transaksi, dan status 
transaksi. Selain rincian produk, halaman ini juga memberikan gambaran 
lengkap mengenai jumlah yang telah dibayarkan. Ini termasuk total biaya 
transaksi.
- Halaman akun pengguna merupakan sebuah halaman di 
dalam sebuah yang memuat informasi dan detail mengenai pengguna yang 
terdaftar,
- Halaman kategori merupakan halaman yang 
menampilkan atau menyajikan produk berdasarkan kategori produk tertentu
yang dijual di website.
- Halaman produk merupakan halaman yang menyajikan 
produk beserta informasi mengenai produk tertentu.
- Halaman keranjang merupakan halaman yang akan 
tampil sebelum pelanggan melakukan checkout. 
- Halaman pembayaran merupakan halaman yang tampil 
setelah pelanggan melakukan checkout.
- Halaman admin dashboard menampilkan informasi secara singkat mengenai jumlah user 
yang terdaftar, penghasilan, dan transaksi yang berhasil dilaksanakan.
- Halaman admin produk melibatkan proses 
menambahkan informasi produk ke dalam sistem atau basis data website, 
sehingga produk tersebut dapat ditampilkan dan diakses oleh pengguna.
- Pada admin galeri merupakan halaman penginputan galeri produk merujuk 
pada proses menambahkan gambar-gambar yang berkaitan dengan suatu 
produk ke dalam sistem atau basis data website.
- Halaman admin kategori  merupakan halaman khusus 
digunakan untuk mengelola kategori produk.
- Halaman admin transaksi pada digunakan untuk mengelola 
dan melihat informasi terkait dengan transaksi atau pesanan yang dilakukan 
oleh pengguna website. 
- Halaman ini merupakan halaman yang digunakan untuk mengelola 
pengguna yang terdaftar dalam sistem. 

## Usage
Cara kerja sistem ini menggunakan framework laravel.



## Dokumentasi lebih lanjut
https://drive.google.com/file/d/1VA18IV0kYbNgaYgcxyFkZa_rPJlF_Xdx/view?usp=sharing
