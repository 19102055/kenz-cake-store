@extends('layouts.app')

@section('title')
    Store Cart Page
@endsection

@section('content')
    <!-- Page Content -->
    <div class="page-content page-cart">
        <section class="store-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    Cart
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>

        <section class="store-cart">
            <div class="container">
                <div class="row" data-aos="fade-up" data-aos-delay="100">
                    <div class="col-12 table-responsive">
                        <table class="table table-borderless table-cart">
                            {{-- tabel pada cart --}}
                            <thead>
                                <tr>
                                    <td>Image</td>
                                    <td>Name &amp; Seller</td>
                                    <td>Price</td>
                                    <td>Menu</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php $totalPrice = 0 @endphp
                                @php $totalWeight = 0 @endphp
                                @foreach ($carts as $cart)
                                    <tr>
                                        <td style="width: 20%;">
                                            {{-- mengambil gambar --}}
                                            @if ($cart->product->galleries)
                                                <img src="{{ Storage::url($cart->product->galleries->first()->photos) }}"
                                                    alt="" class="cart-image" />
                                            @endif
                                        </td>
                                        <td style="width: 35%;">
                                            {{-- mengambil nama produk --}}
                                            <div class="product-title">{{ $cart->product->name }}</div>
                                            <div class="product-subtitle">by {{ $cart->product->user->store_name }}</div>
                                        </td>
                                        <td style="width: 35%;">
                                            {{-- mengambil harga --}}
                                            <div class="product-title">Rp{{ number_format($cart->product->price) }}</div>
                                            <div class="product-subtitle">Rupiah</div>
                                        </td>
                                        <td style="width: 20%;">
                                            {{-- untuk menghapus --}}
                                            <form action="{{ route('cart-delete', $cart->id) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-remove-cart" type="submit">
                                                    Remove
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    {{-- script kalkulasi --}}
                                    @php $totalPrice += $cart->product->price @endphp
                                    @php $totalWeight += $cart->product->weight @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" data-aos="fade-up" data-aos-delay="150">
                    <div class="col-12">
                        <hr />
                    </div>
                    <div class="col-12">
                        <h2 class="mb-4">Shipping Details</h2>
                    </div>
                </div>
                {{-- form sebelum checkout --}}
                <form action="{{ route('checkout') }}" id="formcheckout" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="total_price" v-model="total" value="{{ $totalPrice }}">
                    <input type="hidden" name="shipping_price" v-model="shippingcost" value="0">
                    <div class="row mb-2" id="locations">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_one">Address 1</label>
                                <input type="text" class="form-control" id="address_one" name="address_one" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_two">Address 2</label>
                                <input type="text" class="form-control" id="address_two" name="address_two" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="provinces_id">Province</label>
                                <select name="provinces_id" id="provinces_id" class="form-control" v-model="provinces_id"
                                    v-if="provinces">
                                    <option v-for="province in provinces" :value="province.id">@{{ province.name }}
                                    </option>
                                </select>
                                <select v-else class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="regencies_id">City</label>
                                <select name="regencies_id" id="regencies_id" class="form-control" v-model="regencies_id"
                                    v-if="regencies">
                                    <option v-for="regency in regencies" :value="regency.id">@{{ regency.name }}
                                    </option>
                                </select>
                                <select v-else class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="zip_code">Postal Code</label>
                                <input type="text" class="form-control" id="zip_code" name="zip_code" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" id="country" name="country"
                                    value="Indonesia" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone_number">Mobile</label>
                                <input type="text" class="form-control" id="phone_number" name="phone_number"
                                    value="" />
                            </div>
                        </div>
                        <div class="col-12">
                            {{-- ekspedisi --}}
                            <h2>Courier</h2>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="courier" id="jne"
                                    value="jne" v-model="courier" checked>
                                <label class="form-check-label" for="jne">
                                    JNE
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="courier" id="tiki"
                                    value="tiki" v-model="courier">
                                <label class="form-check-label" for="tiki">
                                    TIKI
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="courier" id="pos"
                                    value='pos' v-model="courier">
                                <label class="form-check-label" for="pos">
                                    Pos Indonesia
                                </label>
                            </div>
                        </div>

                        <div class="col-12" v-if="shippingcosts">
                            <h2>Service</h2>
                            <div v-for="cost in shippingcosts" class="mb-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" :value="cost.cost"
                                        v-model="shippingcost" checked>
                                    <label class="form-check-label">
                                        @{{ `${cost.name} (delivery in ${cost.etd} days)` }}
                                    </label>
                                </div>
                            </div>
                            <p>Shipping Cost : <span class="text-success">@{{ `Rp${shippingcost}` }}</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <hr />
                        </div>
                        <div class="col-12">
                            <h2 class="mb-1">Payment Informations</h2>
                        </div>
                    </div>
                    <div class="row">
                        {{-- total pembayaran --}}
                        <div class="col-4 col-md-2">
                            <div class="product-title text-success">@{{ "Rp" + total }}</div>
                            <div class="product-subtitle">Total</div>
                        </div>
                        <div class="col-8 col-md-3">
                            <button type="submit" class="btn btn-success mt-4 px-4 btn-block">
                                Checkout Now
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection

@push('addon-script')
    <script src="/vendor/vue/vue.js"></script>
    <script src="https://unpkg.com/vue-toasted"></script>
    <script src="https://unpkg.com/axios@1.1.2/dist/axios.min.js"></script>
    <script>
        var locations = new Vue({
            el: "#formcheckout",
            mounted() {
                this.getProvincesData();
            },
            data: {
                provinces: null,
                regencies: null,
                provinces_id: null,
                regencies_id: null,
                shippingcosts: null,
                courier: "jne",
                shippingcost: 0,
                weight: {{ $totalWeight }},
                totalPrice: {{ $totalPrice }},
                total: {{ $totalPrice }},
            },
            methods: {
                getProvincesData() {
                    var self = this;
                    axios.get('{{ route('api-provinces') }}')
                        .then(function(response) {
                            self.provinces = response.data;
                        })
                },
                getRegenciesData() {
                    var self = this;
                    axios.get('{{ url('api/regencies') }}/' + self.provinces_id)
                        .then(function(response) {
                            self.regencies = response.data;
                        })
                },
                getShippingCost() {
                    var self = this;
                    axios.post('{{ route('api-shipping-cost') }}', {
                            origin: 41,
                            destination: self.regencies_id,
                            courier: self.courier,
                            weight: self.weight,
                        })
                        .then(function(response) {
                            console.log(response.data);
                            self.shippingcosts = response.data;
                        })
                },
            },
            watch: {
                provinces_id: function(val, oldVal) {
                    this.regencies_id = null;
                    this.getRegenciesData();
                },
                courier: function(val, oldVal) {
                    if (this.regencies_id === null) {
                        alert("Pilih kota terlebih dahulu!");
                    } else {
                        this.getShippingCost();
                    }
                },
                regencies_id: function(val, oldVal) {
                    this.getShippingCost();
                },
                shippingcost: function(val, oldVal) {
                    this.total = this.totalPrice + val;
                },
            }
        });
    </script>
@endpush
