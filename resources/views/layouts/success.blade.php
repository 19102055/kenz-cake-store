<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>@yield('title')</title>

    <!-- style -->
    @method('prepend-style')
    @include('includes.style')
    @method('addon-style')


  </head>

  <body>

    <!-- konten page/karosel -->
    @yield('content')
    <!-- footer -->
    @include('includes.footer')
    <!--  Script Bootstrap core JavaScript -->
    @method('prepend-script')
    @include('includes.script')
    @method('addon-script')
  </body>
</html>
