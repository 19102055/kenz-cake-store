<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MidtransWebhookController extends Controller
{
    //
    public function post(Request $req)
    {
        try {
            $notification_body = json_decode($req->getContent(), true);
            Log::info($req->getContent());
            $invoice = $notification_body['order_id'];
            // $transaction_id = $notification_body['transaction_id'];
            $status_code = $notification_body['status_code'];
            $order = Transaction::where('code', $invoice)->first();
            if (!$order) return ['code' => 0, 'message' => 'Terjadi kesalahan | Pembayaran tidak valid'];
            switch ($status_code) {
                case '200':
                    $order->transaction_status = "SUCCESS";
                    break;
                case '201':
                    $order->transaction_status = "PENDING";
                    break;
                case '202':
                    $order->transaction_status = "CANCEL";
                    break;
            }
            $order->save();
            return response('Ok', 200)->header('Content-Type', 'text/plain');
        } catch (\Exception $e) {
            return response('Error', 404)->header('Content-Type', 'text/plain');
        }
    }
}
