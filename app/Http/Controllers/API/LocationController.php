<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;
use Illuminate\Support\Facades\Http;

class LocationController extends Controller
{
    public function provinces(Request $request)
    {
        $response = Http::get('https://api.rajaongkir.com/starter/province?key=' . config("services.rajaongkir.key"));
        $data = collect($response['rajaongkir']['results']);
        return $data->map(function ($item) {
            return [
                'id' => $item['province_id'],
                'name' => $item['province']
            ];
        });
    }

    public function regencies(Request $request, $provinces_id)
    {
        $response = Http::get('https://api.rajaongkir.com/starter/city?key=' . config("services.rajaongkir.key") . '&province=' . $provinces_id);
        $data = collect($response['rajaongkir']['results']);
        return $data->map(function ($item) {
            return [
                'id' => $item['city_id'],
                'name' => $item['city_name']
            ];
        });
    }

    public function shipping_cost(Request $request)
    {
        $response = Http::post('https://api.rajaongkir.com/starter/cost', [
            'key' => config("services.rajaongkir.key"),
            'origin' => $request->origin,
            'destination' => $request->destination,
            'weight' => $request->weight,
            'courier' => $request->courier
        ]);
        //return $response['rajaongkir']['results'][0]['costs'];
        $data = collect($response['rajaongkir']['results'][0]['costs']);

        return $data->map(function ($item) {
            return [
                'name' => $item['service'],
                'description' => $item['description'],
                'cost' => $item['cost'][0]['value'],
                'etd' => $item['cost'][0]['etd']
            ];
        });
    }
}
